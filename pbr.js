var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(
  75, window.innerWidth / window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.autoClear = false;
renderer.setPixelRatio( window.devicePixelRatio );
renderer.setFaceCulling( THREE.CullFaceNone );
document.body.appendChild( renderer.domElement );

camera.position.z = 5;

var controls = new THREE.OrbitControls( camera );
controls.minDistance = 1.5;
controls.maxDistance = 7;

var ambient = new THREE.AmbientLight( 0xffffff );
scene.add( ambient );

var textureLoader = new THREE.TextureLoader();
var jsonLoader = new THREE.JSONLoader();
var shaderLoader = new THREE.XHRLoader();
var imageLoader = new THREE.ImageLoader();

var radianceTexture, irradianceTexture;
var monkeyMaterial, monkeyMesh, monkeyVertexShader, monkeyFragmentShader;

function setupMonkeyMaterial()
{
  if ( monkeyVertexShader && monkeyFragmentShader && radianceTexture &&
    irradianceTexture )
  {
    monkeyMaterial = new THREE.ShaderMaterial(
    {
      uniforms:
      {
        envmap:
        {
          type: 't',
          value: radianceTexture
        },
        irradianceMap:
        {
          type: 't',
          value: irradianceTexture
        },
      },
      vertexShader: monkeyVertexShader,
      fragmentShader: monkeyFragmentShader,
    } );

    setupMonkey();
  }
}

function setupMonkey()
{
  if ( monkeyMaterial && monkeyMesh )
  {
    var object = new THREE.Mesh( monkeyMesh, monkeyMaterial );
    scene.add( object );
  }
}

textureLoader.load( 'irradiance.png', function( texture )
{
  texture.format = THREE.RGBFormat;
  texture.mapping = THREE.EquirectangularReflectionMapping;
  texture.magFilter = THREE.LinearFilter;
  texture.minFilter = THREE.LinearFilter;

  var equirectShader = THREE.ShaderLib[ "equirect" ];
  var equirectMaterial = new THREE.ShaderMaterial(
  {
    fragmentShader: equirectShader.fragmentShader,
    vertexShader: equirectShader.vertexShader,
    uniforms: equirectShader.uniforms,
    depthWrite: false,
    side: THREE.BackSide
  } );
  equirectMaterial.uniforms[ "tEquirect" ].value = texture;
  var cubeMesh = new THREE.Mesh( new THREE.BoxGeometry( 100, 100, 100 ),
    equirectMaterial );
  scene.add( cubeMesh );
  irradianceTexture = texture;
  setupMonkeyMaterial();
} );

shaderLoader.load( 'pbr_vert.glsl', function( text )
{
  monkeyVertexShader = text;
  setupMonkeyMaterial();
});

shaderLoader.load( 'pbr_frag.glsl', function( text )
{
  monkeyFragmentShader = text;
  setupMonkeyMaterial();
});

var mipmaps = new Array();

function onUpdateRadianceTexture()
{
  for ( var i = 0; i < 7; i++ )
  {
    radianceTexture.mipmaps[i] = mipmaps[i];
  }
  radianceTexture.needsUpdate = true;
}

function setupRadianceTexture()
{
  if ( mipmaps.length == 8 )
  {
    radianceTexture = new THREE.Texture( mipmaps[0] );

    radianceTexture.needsUpdate = true;
    radianceTexture.onUpdate = onUpdateRadianceTexture;

    setupMonkeyMaterial();
  }
}
imageLoader.load( 'radiance0.png', function( image )
{
  mipmaps[0] = image;
  setupRadianceTexture();
} );
imageLoader.load( 'radiance1.png', function( image )
{
  mipmaps[1] = image;
  setupRadianceTexture();
} );
imageLoader.load( 'radiance2.png', function( image )
{
  mipmaps[2] = image;
  setupRadianceTexture();
} );
imageLoader.load( 'radiance3.png', function( image )
{
  mipmaps[3] = image;
  setupRadianceTexture();
} );
imageLoader.load( 'radiance4.png', function( image )
{
  mipmaps[4] = image;
  setupRadianceTexture();
} );
imageLoader.load( 'radiance5.png', function( image )
{
  mipmaps[5] = image;
  setupRadianceTexture();
} );
imageLoader.load( 'radiance6.png', function( image )
{
  mipmaps[6] = image;
  mipmaps[7] = image;
  setupRadianceTexture();
} );

jsonLoader.load(
  "monkey.js",
  function( geometry, materials )
  {
    monkeyMesh = geometry;
    setupMonkey();
  } );

function onWindowResize()
{
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );
}
window.addEventListener( "resize", onWindowResize, false );

function render()
{
  requestAnimationFrame( render );

  controls.update();
  camera.lookAt( scene.position );
  renderer.render( scene, camera );
}
render();
