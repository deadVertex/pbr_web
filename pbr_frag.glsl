#define RECIPROCAL_PI2 0.15915494
#define PI 3.1415926535897932384626433832795
varying vec2 vUv;
varying vec3 vPosition;
varying vec3 vNormal;
varying vec3 vWorldPosition;
uniform sampler2D envmap;
uniform sampler2D irradianceMap;

vec3 lerp( vec3 a, vec3 b, float t )
{
  return a * ( 1.0 - t ) + t * b;
}

vec3 FresnelSchlick( vec3 F0, float cosT, float power )
{
  return F0 + ( vec3( 1.0 ) - F0 ) * pow( 1.0 - cosT, power );
}

vec4 textureEquirect( sampler2D texture, vec3 V )
{
  vec2 sampleUV;
  sampleUV.y = clamp( V.y * -0.5 + 0.5, 0.0, 1.0 );
  sampleUV.x = atan( V.z, V.x ) * RECIPROCAL_PI2 + 0.5;
  return texture2D( texture, sampleUV );
}

vec4 textureEnvMap( sampler2D texture, vec3 V, float lod )
{
  vec2 sampleUV;
  sampleUV.y = clamp( V.y * -0.5 + 0.5, 0.0, 1.0 );
  sampleUV.x = atan( V.z, V.x ) * RECIPROCAL_PI2 + 0.5;
  return texture2D( texture, sampleUV, lod );
}

vec3 MetallicBrdf( vec3 N, vec3 V, vec3 specularColour, float roughness,
                   float metallic )
{
  vec3 R = reflect( V, N );

  vec3 reflection = textureEnvMap( envmap, R, roughness * 6.0 ).rgb;
  float cosTheta = clamp( dot( N, V ), 0.0, 1.0 );
  vec3 reflectionColour = lerp( vec3( 1.0 ), specularColour, metallic );
  return FresnelSchlick( specularColour, cosTheta, 5.0 ) * reflection *
    reflectionColour;
}

vec3 DielectricBrdf( vec3 N, vec3 V, vec3 diffuseColour, vec3 specularColour, float metallic )
{

  vec3 irradiance = textureEquirect( irradianceMap, -N ).rgb;
  return diffuseColour * irradiance;
}

vec3 LambertDiffuse( vec3 colourDiffuse )
{
  return colourDiffuse / PI;
}

float ComputeGeometric( vec3 N, vec3 H, vec3 V )
{
  float nDotV = max( 0.0, dot( N, V ) );
  float nDotH = max( 0.0, dot( N, H ) );
  float vDotH = max( 0.0, dot( V, H ) );
  return min( 1.0, ( 2.0 * nDotH * nDotV ) / vDotH );
}

float BeckmannDistribution( vec3 N, vec3 H, float m )
{
  float nDotH = max( 0.0001, dot( N, H ) );
  float a = 1.0 / ( 4.0 * m * m * pow( nDotH, 4.0 ) );
  float b = nDotH * nDotH - 1.0;
  float c = m * m * nDotH * nDotH;

  return a * exp( b / c );
}

vec3 CookTorranceSpecular( vec3 L, vec3 N, vec3 V, vec3 H, vec3 colourSpec,
                           float roughness )
{
  float facing = clamp( dot( V, N ), 0.0, 1.0 );
  vec3 fresnel = FresnelSchlick( colourSpec, facing, 5.0 );
  float roughnessTerm = BeckmannDistribution( N, H, roughness );
  float geometric = ComputeGeometric( N, H, V );
  vec3 cookTorrance =
    ( fresnel * roughnessTerm * geometric ) /
    ( max( 0.0001, dot( N, V ) ) * max( 0.0001, dot( N, L ) ) );
  return colourSpec * cookTorrance;
}
vec3 DirectionalLight( vec3 L, vec3 N, vec3 V, vec3 lightIrradiance,
                       vec3 colourDiffuse, vec3 colourSpec,
                       float roughness, vec3 position )
{
  float cosTheta = max( 0.0, dot( N, L ) );
  vec3 H = normalize( V + L );
  return ( LambertDiffuse( colourDiffuse ) +
           CookTorranceSpecular( L, N, V, H, colourSpec, roughness ) ) *
           lightIrradiance * cosTheta;
}

void main()
{
  vec3 N = normalize( vNormal );
  vec3 V = normalize( cameraPosition - vPosition );

  vec3 specularColour = vec3( 0.04 );
  vec3 diffuseColour = vec3( 0.1 );
  float metallic = 0.0;
  float roughness = 0.01;
  vec3 sunDir = -normalize( vec3( -0.2, -1, -0.8 ) );
  vec3 sunColour = vec3( 1.0 ) * 1.0;


  specularColour = lerp( specularColour, diffuseColour, metallic );
  diffuseColour = lerp( diffuseColour, vec3( 0.0 ), metallic );

  vec3 color = MetallicBrdf( N, V, specularColour, roughness, metallic );
  if ( metallic < 1.0 )
  {
    vec3 irradiance = textureEquirect( irradianceMap, -N ).rgb;
    color += diffuseColour * irradiance;

    color += DirectionalLight( sunDir, N, V, sunColour, diffuseColour,
                               specularColour, roughness, vPosition );
  }
  gl_FragColor = vec4( color, 1.0 );
}
